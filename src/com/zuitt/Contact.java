package com.zuitt;

public class Contact {

    protected String name;

    protected String homeContactNumber;
    protected String officeContactNumber;

    protected String homeAddress;

    protected String officeAddress;


    public Contact(){

    }

    public Contact(String name, String homeContactNumber, String officeContactNumber, String homeAddress, String officeAddress){
        this.name = name;
        this.homeContactNumber= homeContactNumber;
        this.officeContactNumber= officeContactNumber;
        this.homeAddress = homeAddress;
        this.officeAddress = officeAddress;
    }

    public String getName(){
        return this.name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getHomeContactNumber() {
        return homeContactNumber;
    }

    public void setHomeContactNumber(String homeContactNumber) {
        this.homeContactNumber = homeContactNumber;
    }

    public String getOfficeContactNumber() {
        return officeContactNumber;
    }

    public void setOfficeContactNumber(String officeContactNumber) {
        this.officeContactNumber = officeContactNumber;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getOfficeAddress() {
        return officeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        this.officeAddress = officeAddress;
    }
}
