import com.zuitt.Phonebook;
import com.zuitt.Contact;
public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact();
        Contact contact2 = new Contact();

        contact1.setName("John Doe");
        contact1.setHomeContactNumber("+639152468596");
        contact1.setOfficeContactNumber("+63945123133");
        contact1.setHomeAddress("Quezon city");
        contact1.setOfficeAddress("marikina city");

        contact2.setName("Jane Doe");
        contact2.setHomeContactNumber("+639152468884");
        contact2.setOfficeContactNumber("+639162148573");
        contact2.setHomeAddress("Caloocan City");
        contact2.setOfficeAddress("Pasay City");

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);


        if(phonebook.getContacts().size() == 0){
            System.out.println("The phonebook is empty");
        }else{
            phonebook.getContacts().forEach(contact -> {
                System.out.println(contact.getName());
                System.out.println("--------------------");
                System.out.println(contact.getName() + " has the following registered numbers:");
                System.out.println(contact.getHomeContactNumber());
                System.out.println(contact.getOfficeContactNumber());
                System.out.println("----------------------------");
                System.out.println(contact.getName() + " has the following registered addresses:");
                System.out.println(contact.getHomeAddress());
                System.out.println(contact.getOfficeAddress());
                System.out.println("=======================================");
            });
        }



    }
}